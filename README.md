# Jet Factory

Create live and flashable linux distribution root filesystem images.

## Dependencies

Ubuntu 19.10:

```txt
sudo apt-get install jq qemu qemu-user-static binfmt-support arch-install-scripts wget p7zip-full xz-utils zerofree libarchive-tools curl debootstrap
```

## Usage

```txt
Usage: ./jet-factory -d <device> -o <distribution_name> <output_directory>
Create a distribution from configuration set in JSON

-h, --help                  	Display help 
-b, --bootstrap		    	(optional) Create rootfs archive ONLY (using config set in JSON)
-c, --chroot			(optional) Chroot into given directory, register QEMU binaries if needed
-d, --device			Device name as set in configs/ directory
-k, --hekate			(optional) Build with hekate format (7zip flashable archive)
-o, --os			OS to bootstrap
-r, --register              	(optional) Register QEMU binaries
-t, --type			(optional) Filesystem type for the output image
-v, --verbose               	(optional) Run script in verbose mode. Will print out each step of execution.
```

### Docker usage

```sh
sudo docker run --privileged --rm -it -e DISTRO=arch -e DEVICE=switch -v "$PWD"/linux:/out registry.gitlab.com/switchroot/gnu-linux/jet-factory:latest
```

### Docker tips

*You can override the workdir used in the docker, to use your own changes, without rebuilding the image by adding this repository directory as a volume to the docker command above.*

```sh
-v $(pwd):/build
```

## Credits

### Special mentions

@gavin_darkglider, @CTCaer, @ByLaws, @ave and all the [switchroot team members](https://switchroot.org) \
For their various work and contributions to switchroot.

### Contributors

@Stary2001, @Kitsumi, @parkerlreed, @AD2076, @PabloZaiden, @andrebraga1 @nicman23 @Matthew-Beckett \
For their work, support and direct contribution to this project.
