#!/bin/bash

# Retrieve output path, should always be last argument
export out=$(realpath "${@:$#}")

# Store the script directory
cwd="$(dirname "$(readlink -f "$0")")"

# Supported image file format
images_format=(".raw .img .iso .qcow2")

# Set download dir
download_dir="${out}/downloadedFiles/"

setVars() {
	echo -e "\nBuilding ${DISTRO} for device ${DEVICE}"

	if [[ ! -d "${out}" ]]; then
		echo -e "\n${out} is not a valid directory! Exiting.."
		exit 1
	fi

	# Check if distribution is based on another configuration
	BASE=$(jq -r '.'${DISTRO}'.base' < ${cwd}/configs/${DEVICE}.json)
	if [[ -n ${BASE} && ${BASE} != "null" ]]; then

		echo "Using ${BASE} config as base"

		# If SKIP_BASE is set then skip BASE chroot_script
		SKIP_BASE=$(jq -r '.'${DISTRO}'.skip_base_script' < ${cwd}/configs/${DEVICE}.json)

		if [[ ${SKIP_BASE} != "true" ]]; then
			CHROOT_SCRIPT=$(jq -r '.'${BASE}'.chroot_script' < ${cwd}/configs/${DEVICE}.json |  sed '0,/\[/s/\[//g; $s/\]//g; s/,$//g; s/"//g')
		fi

		CHROOT_SCRIPT="${CHROOT_SCRIPT}$(jq -r '.'${DISTRO}'.chroot_script' < ${cwd}/configs/${DEVICE}.json |  sed '0,/\[/s/\[//g; $s/\]//g; s/,$//g; s/"//g')"

		# Check if debootstrap should be used to create the base image
		DEBIAN=$(jq -r '.'${DISTRO}'.debootstrap' < ${cwd}/configs/${DEVICE}.json)
		if [[ -z ${DEBIAN} || ${DEBIAN} == "null" ]]; then

			# Check if a custom URL has been specified via environment or argument
			if [[ ${URL} == "null" || -z ${URL} ]]; then

				# Check if DISTRO has already an URL set
				URL=$(jq -r '.'${DISTRO}'.url' < ${cwd}/configs/${DEVICE}.json)

				# Otherwise use the BASE URL
				if [[ ${URL} == "null" || -z ${URL} ]]; then
					echo "Using ${BASE} image url"
					URL=$(jq -r '.'${BASE}'.url' < ${cwd}/configs/${DEVICE}.json)
				else
					echo "Using ${DISTRO} image url"
				fi
			fi

			# Check for a image signature
			SIG=$(jq -r '.'${DISTRO}'.sig' < ${cwd}/configs/${DEVICE}.json)
			if [[ ${SIG} == "null" || -z ${SIG} ]]; then
				echo "Using ${BASE} signature"
				SIG=$(jq -r '.'${BASE}'.sig' < ${cwd}/configs/${DEVICE}.json)
			fi
		fi

		# Check for a filesystem format
		TYPE=$(jq -r '.'${DISTRO}'.type' < ${cwd}/configs/${DEVICE}.json)
		if [[ ${TYPE} == "null" || -z ${TYPE} ]]; then
			echo "Using ${BASE} filesystem type"
			TYPE=$(jq -r '.'${BASE}'.type' < ${cwd}/configs/${DEVICE}.json)
		fi

		# Check for the presence of a cache directory
		CACHE_DIR=$(jq -r '.'${DISTRO}'.cache' < ${cwd}/configs/${DEVICE}.json)
		if [[ ${CACHE_DIR} == "null" || -z ${CACHE_DIR} ]]; then
			echo "Using ${BASE} cache dir"
			CACHE_DIR=$(jq -r '.'${BASE}'.cache' < ${cwd}/configs/${DEVICE}.json)
		fi
	else
		DEBIAN=$(jq -r '.'${DISTRO}'.debootstrap' < ${cwd}/configs/${DEVICE}.json)
		if [[ -z ${DEBIAN} || ${DEBIAN} == "null" ]]; then
			URL=$(jq -r '.'${DISTRO}'.url' < ${cwd}/configs/${DEVICE}.json)
			SIG=$(jq -r '.'${DISTRO}'.sig' < ${cwd}/configs/${DEVICE}.json)
		fi

		TYPE=$(jq -r '.'${DISTRO}'.type' < ${cwd}/configs/${DEVICE}.json)
		CACHE_DIR=$(jq -r '.'${DISTRO}'.cache' < ${cwd}/configs/${DEVICE}.json)
		CHROOT_SCRIPT=$(jq -r '.'${DISTRO}'.chroot_script' < ${cwd}/configs/${DEVICE}.json |  sed '0,/\[/s/\[//g; $s/\]//g; s/,$//g; s/"//g')
	fi

	# Check how to build bootfiles
	BOOT=$(jq -r '.boot' < ${cwd}/configs/${DEVICE}.json |  sed '0,/\[/s/\[//g; $s/\]//g; s/,$//g; s/"//g')
	if [[ -z "${URL}" || ${URL} == "null" ]]; then
		if [[ ${DEBIAN} == "null" && -z ${DEBIAN} ]]; then
			echo -e "\nNo URL or other way to create the initial root found. Exiting."
			exit 1
		fi
	fi

	if [[ -z "${CHROOT_SCRIPT}" || ${CHROOT_SCRIPT} == "null" ]]; then
		echo -e "\nNo CHROOT_SCRIPT found. Exiting."
		exit 1
	fi

	if [[ -z "${TYPE}" || ${TYPE} == "null" ]]; then
		TYPE="ext4"
		echo -e "\nNo filesystem type set, using ext4 type as default"
	fi
}

prepare() {
	# Set build and download dirs
	rootfs="${out}/.${DEVICE}-${DISTRO}"
	download_dir="${out}/downloadedFiles/"

	# Clean previously made DISTRO
	rm -rf "${rootfs}"

	mkdir -p "${rootfs}" "${download_dir}"

	if [[ ${DEBIAN} != "null" && -n ${DEBIAN} ]]; then
		debootstrap --arch ${DEBIAN} ${DISTRO} ${rootfs}
	else
		# Retrieve image name from URL
		img="${URL##*/}"
		
		if [[ ! -e "${download_dir}/${img%.*}" && -n ${URL} && ${URL} != "null" ]]; then
			wget -q -nc --show-progress "${URL}" -P "${download_dir}"
		fi

		if [[ -n "${SIG}" && ${SIG} != "null" ]]; then
			wget -q --show-progress "${SIG}" -P "${download_dir}"

			echo -e "\nChecking file integrity"
			if [[ "${SIG}" =~ .md5 ]]; then
				md5sum --status -c "${download_dir}/${SIG##*/}"
			else
				shasum --status -c "${download_dir}/${SIG##*/}"
			fi
		fi

		echo -e "\nExtracting rootfs from disk image or archive. This might take a while..."

		# Set path of image
		img="${download_dir}/${img}"
		
		# Check if rootfs is an disk image
		for format in $images_format; do
			if [[ ${img} = *$format || ${img%.*} = *$format ]]; then
				format=$format
				is_iso=1; break
			fi
		done

		if [[ -n ${is_iso} ]]; then
			# Handle qcow2 conversion
			if [[ ${format} == ".qcow2" ]]; then
				echo -e "\nConverting qcow2 to raw image"
				qemu-img convert -p -O raw ${img} ${img/qcow2/raw}
				img="${img/qcow2/raw}"
			fi

			# Handle xz compressed images
			if [[ "$(file -b --mime-type "${img}")" == "application/x-xz" ]]; then
				# Check if extracted image doesn't already exist
				[[ ! -e "${img%.*}" ]] && unxz "${img}"
				# Reset image name to extracted image name 
				img=${img%.*};
			fi
		
			# Check if image retrieve from earlier isn't already the extracted one
			[[ ! -e "${img}" ]] && img=${img%.*};

			# Extracting rootfs from image file (Credits to : https://github.com/moby/moby/issues/27886#issuecomment-417074845 for losetup in docker)
			LOOPDEV="$(losetup --show -f -P "$img")"
			PARTITIONS=$(lsblk --raw --output "MAJ:MIN" --noheadings ${LOOPDEV} | tail -n +2)
			COUNTER=1
			for i in $PARTITIONS; do
			    MAJ=$(echo $i | cut -d: -f1)
			    MIN=$(echo $i | cut -d: -f2)
			    if [ ! -e "${LOOPDEV}p${COUNTER}" ]; then mknod ${LOOPDEV}p${COUNTER} b $MAJ $MIN; fi
			    COUNTER=$((COUNTER + 1))
			done
		
			for part in "$LOOPDEV"?*; do
				if [ "$part" = "${LOOPDEV}p*" ]; then
					part="${LOOPDEV}"
				fi
				
				dst="${out}/$(basename "$part")"
				mkdir -p "$dst"
				mount "$part" "$dst"

				echo -e "\nMounting $part in $dst"
				dst=`find ${dst} -name usr -type d -print -quit | sed 's/usr//g'`
				if [[ -n "${dst}" ]]; then
					echo "Copying root from ${dst}"
					cp -a "${dst}"/* "${rootfs}"
				fi
				umount "$dst"
			done
			losetup -d "$LOOPDEV"
		fi

		# Handle tbz2 archives
		if [[ "${img}" = *.tbz2 ]]; then
			tar xpf "${img}" -C "${rootfs}"
		fi

		# Handle tar.* archives
		if [[ "${img}" =~ .tar ]]; then
			bsdtar xpf "${img}" -C "${rootfs}"
		fi
	fi
}

registerQemuBinary() {
	echo -e "\nRegistering QEMU binary if needed"
	
	if [[ `ls /usr/bin/qemu-*-static` ]]; then
		suffix="--qemu-suffix=-static"
	fi

	# Register binary if the host and target CPU architectures differ
	find /proc/sys/fs/binfmt_misc -type f -name 'qemu-*' -exec sh -c 'echo -1 > {}' \;
	curl -s https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh | bash -s -- -c yes -p yes --qemu-path=/usr/bin ${suffix}
}

bootstrap() {
	echo -e "\nPreparing filesystem for chroot"

	# Create cache variable
	CACHE="${out}/.cache/${DEVICE}-${DISTRO}"

	# Check if target and host architecture are the same
	AARCH=$(file -bL ${1}/sbin/init | cut -d, -f2 | sed 's/.* //')
	if [[ "$(uname -m)" != "${AARCH}" ]]; then
		registerQemuBinary
	fi

	# Handle resolv.conf
	cp --remove-destination /etc/resolv.conf "${1}/etc/"

	# Add cache dir configuration
	if [[ -n "${CACHE_DIR}" && ${CACHE_DIR} != "null" ]]; then
		mkdir -p "${CACHE}"
		mount --bind "${CACHE}" "${rootfs}/${CACHE_DIR}"
	fi

	# Bind directory to itself for chroot
	mount --bind "${1}" "${1}"

	IFS=$'\n'
	if [[ -n "${CHROOT_SCRIPT}" ]]; then
		arch-chroot "${1}" /bin/sh <<<"$CHROOT_SCRIPT" || exit 1
	else
		arch-chroot "${1}" /bin/sh 
	fi

	umount -R "${1}"
}

createDiskImage() {
	echo -e "\nCreating disk image file"
	dd if=/dev/zero of="${1}" count=${2} bs=1M
	mkfs.${TYPE} "${1}"
}

copyToImage() {
	echo -e "\n Copying build files to disk image, this will take a while..."
	temp=${out}/.tmp-chroot-${DEVICE}-${DISTRO}

	# Create mountpoint and mount image in it
	mkdir -p "${temp}"
	mount "${2}" "${temp}"

	# Copy to mountpoint
	cp -a "${1}"/* "${temp}"

	# Unmount and remove mountpoint
	umount -l "${temp}"
	rm -rf "${temp}"

	# Zerofree the image
	zerofree -n "${2}"
}

build() {
	# Set distro with date
	distro_with_date=${DISTRO}-$(date '+%Y-%m-%d')

	# Image name 
	guestfs_img="${out}/switchroot-${distro_with_date}.img"

	# 7zip name
	zip_final="${out}/switchroot-${distro_with_date}.7z"

	bootstrap "${rootfs}"

	# Get rootfs final size
	size=$(du -sBM "${rootfs}" | awk '{ print $1+1024 }')

	# Create disk image: createDiskImage <image_name> <size>
	createDiskImage "${guestfs_img}" "${size}"
	
	# Copy file to image: copyToImage <directory_containing_files> <image_file>
	copyToImage "${rootfs}" "${guestfs_img}"
	
	IFS=$'\n'
	if [[ -n ${BOOT} && ${BOOT} != null ]]; then
		for cmd in ${BOOT}; do
			bash -c "${cmd}"
		done
	fi

	if [[ "${HEKATE}" != "true" ]]; then
		echo -e "\nDone ! Image resides in ${guestfs_img}"
	else
		echo -e "\nCreating hekate installable partition. This will take a while..."

		# Create HEKATE_ID from distribution name
		HEKATE_ID="SWR-${DISTRO:0:3}"
		
		# Create switchroot install folder
		switchroot_dir="${out}/switchroot/"
		mkdir -p "${switchroot_dir}/install/"

		# Add label to image
		e2label "${guestfs_img}" "${HEKATE_ID^^}"

		# Split parts to output directory
		split -b4290772992 --numeric-suffixes=0 "${guestfs_img}" "${switchroot_dir}/install/l4t."

		# 7zip the folder
		7z a "${zip_final}" $(realpath "${switchroot_dir}") $(realpath "${out}/bootloader")

		# Clean build files
		rm -rf "${guestfs_img}" \
			$(realpath "${switchroot_dir}") \
			$(realpath "${out}/bootloader") \
			"${download_dir}/hekate_ctcaer_${hekate}_Nyx_${nyx}.zip" \
			"${out}/hekate_ctcaer_${hekate}.bin"

		echo -e "\nDone ! Hekate flashable 7zip resides in ${zip_final}"
	fi

	# Sync disks before removal
	sync

	# Finally clean old rootfs
	rm -rf "${rootfs}"
}

showHelp() {
cat << EOF  
Usage: ./jet-factory -d <device> -o <distribution_name> <output_directory>
Create a distribution from configuration set in JSON

-h, --help				Display help
-b, --bootstrap			Create rootfs archive ONLY (using config set in JSON)
-c, --chroot			Chroot into given directory, register QEMU binaries if needed
-d, --device			Device name as set in configs/ directory
-k, --hekate			(optional) Build with hekate format (7zip flashable archive)
-o, --os				OS to bootstrap
-r, --register			Register QEMU binaries
-u, --url				Specify a custom image URL
-t, --type				Filesystem type for the output image
-v, --verbose			Run script in verbose mode. Will print out each step of execution.
EOF
}

SCRIPT_OPTS='hbcd:ko:rt:v'
typeset -A ARRAY_OPTS
ARRAY_OPTS=(
	[help]=h
	[bootstrap]=b
	[chroot]=c
	[device]=d
	[hekate]=k
	[os]=o
	[register]=r
	[url]=u
	[type]=t
	[verbose]=v
)

while getopts ${SCRIPT_OPTS} OPTION ; do
	if [[ "x$OPTION" == "x-" ]]; then
		LONG_OPTION=$OPTARG
		LONG_OPTARG=$(echo $LONG_OPTION | grep "=" | cut -d'=' -f2)
		LONG_OPTIND=-1
		[[ "x$LONG_OPTARG" = "x" ]] && LONG_OPTIND=$OPTIND || LONG_OPTION=$(echo $OPTARG | cut -d'=' -f1)
		[[ $LONG_OPTIND -ne -1 ]] && eval LONG_OPTARG="\$$LONG_OPTIND"
		OPTION=${ARRAY_OPTS[$LONG_OPTION]}
		[[ "x$OPTION" = "x" ]] &&  OPTION="?" OPTARG="-$LONG_OPTION"

		if [[ $( echo "${SCRIPT_OPTS}" | grep -c "${OPTION}:" ) -eq 1 ]]; then
		    if [[ "x${LONG_OPTARG}" = "x" ]] || [[ "${LONG_OPTARG}" = -* ]]; then 
			OPTION=":" OPTARG="-$LONG_OPTION"
		    else
			OPTARG="$LONG_OPTARG";
			if [[ $LONG_OPTIND -ne -1 ]]; then
			    [[ $OPTIND -le $Optnum ]] && OPTIND=$(( $OPTIND+1 ))
			    shift $OPTIND
			    OPTIND=1
			fi
		    fi
		fi
	fi

	if [[ "x${OPTION}" != "x:" ]] && [[ "x${OPTION}" != "x?" ]] && [[ "${OPTARG}" = -* ]]; then 
		OPTARG="$OPTION" OPTION=":"
	fi
	
	case "$OPTION" in
		h) 
			showHelp
			exit 0
			;;
		b)
			setVars
			prepare
			cd "${rootfs}"
			tar czf "${out}/${DEVICE}-${DISTRO}.tar.gz" ./*
			echo Rootfs archive created in "${out}/${DEVICE}-${DISTRO}.tar.gz"
			exit 0
			;;
		c)
			bootstrap "${out}"
			exit 0
			;;
		d)
			export DEVICE="${OPTARG}"
			;;
		k)
			export HEKATE=true
			;;
		o)
			export DISTRO="${OPTARG}"
			;;
		r)
			registerQemuBinary
			exit 0
			;;
		u)
			export URL="${OPTARG}"
			echo "Using custom URL for ${DISTRO}: ${URL}"
			;;
		t)
			export TYPE="${OPTARG}"
			;;
		v)
			set -xv  # Set xtrace and verbose mode.
			;;
	esac
done
shift $((OPTIND-1))

if [[ "$EUID" -ne 0 ]]; then
	echo "Please run as root"
	exit 1
fi

setVars
prepare
build
