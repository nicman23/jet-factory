FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt install -y \
	qemu \
	qemu-user-static \
	binfmt-support \
	wget \
	p7zip-full \
	xz-utils \
	libarchive-tools \
	jq \
	util-linux \
	zerofree \
	arch-install-scripts \
	debootstrap \
	curl \
	file \
	git \
	&& rm -rf /var/lib/apt/lists/*

RUN /bin/bash -c 'set -ex && \
    ARCH=`uname -m` && \
    if [ "$ARCH" != "aarch64" ]; then \
	echo "x86_64" && \
	apt update -y && \
	apt install -y linux-image-generic; \
    fi'


WORKDIR /build
VOLUME /out

ARG DISTRO
ENV DISTRO=${DISTRO}
ARG DEVICE
ENV DEVICE=${DEVICE}
ARG HEKATE
ENV HEKATE=${HEKATE}

COPY configs /build/configs/
COPY jet-factory.sh /build
RUN find /build -type f -iname "*.sh" -exec chmod +x {} \;

CMD ./jet-factory.sh /out
